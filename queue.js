let collection = [];

function print(){return collection}

function isEmpty(){return collection.length ===0}

function size(){return collection.length}

function front(){return collection[0]}

function enqueue(data){
	let ArrLength = collection.length
	collection[collection.length] = data
	return collection
}

function dequeue(){
	let myArr= []
	let ArrLength = collection.length
	for (let i = 0 ; i < (ArrLength -1) ;i++) myArr[i] = collection[i+1]
	return collection = myArr;
}

// No need to deconstruct
//
// 	collection(){
// 		this.elements = [];
// 	    this.head = 0;
// 	    this.tail = 0;

// 	},
// 	print(){
// 		/*for(let i=0;i<this.tail;i++){
// 			return this.elements[i]
// 		}
// */	},
// 	enqueue(element){
// 		/*this.elements[this.tail]=element;
// 		this.tail++;*/
// 		return this.elements.push(element);
// 	},
// 	dequeue(){
// 		const item = this.elements[this.head];
// 	    delete this.elements[this.head];
// 	    this.head++;
// 	    return item;
// 	    if(this.elements.length>0){
// 	    	return this.items.shift()
// 	    }
// 	},
// 	front(){
// 		/*return this.elements[0]*/
// 		return this.elements[this.elements.length - 1];

// 	},
// 	size(){
// 		/*return this.tail - this.head*/
// 		return this.elements.length - 1
// 	},
// 	isEmpty(){
// 		return this.length ===0;
// 	}

	// collection(){
	// 	this.data ={};
	// 	this.rear=0;
	// 	this.size=collection.length;

	// },
	// print(){
	// 	for(let i=0;i<this.rear;i++){
	// 		return this.data[i]
	// 	}
	// },
	// enqueue(element){
	// 	this.data[this.rear]=element;
	// 	this.rear=this.rear+1;
	// },
	// dequeue(){
	// 	if(this.isEmpty()===false){
	// 		this.rear=this.rear-1;
	// 		return this.data.shift();
	// 	}
	// },
	// front(){
	// 	if(collection.isEmpty()===false){
	// 		return collection.data[0]
	// 	}
	// },
	// size(){
	// 	return this.rear;
	// },
	// isEmpty(){
	// 	return this.rear ===0;
	// }

module.exports = {
	collection,
	print,
	enqueue,
	dequeue,
	front,
	size,
	isEmpty
};